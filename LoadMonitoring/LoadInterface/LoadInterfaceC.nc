/*
 *
 * Implementation of Power Measurement Data Transmission application
 * using Wireless Sensor Network.
 *
 * @author: Mark Anthony Cabilo
 * @date  : 2018 
 */

#include <stdlib.h>
#include <math.h> //for modf function
#include "datastruct.h"

module LoadInterfaceC {
	provides interface Msp430UartConfigure;          //for UART configuration
	uses
	{
		//preliminary interfaces
		interface Boot;
		interface SplitControl as AMControl;
		interface Timer<TMilli> as Counter;

#ifdef DEBUG
		interface Leds;
#endif
		//CTP interfaces
		interface StdControl as CollectionControl;
		interface StdControl as DisseminationControl;
		interface LowPowerListening;
		interface Send as DataSender;
		interface Receive;

		//GPIO interfaces
		interface HplMsp430GeneralIO as ReadPort;
		interface HplMsp430Interrupt as Interrupt;
		interface HplMsp430GeneralIO as InPort;
		
		//UART interfaces
		interface UartStream;
		interface Resource;
	}
}

implementation {

	//GLOBAL VARIABLES
	message_t pkt;	                          //transmission packet
	bool busy = FALSE;                        //mote-base transmission flag
	uint8_t rx_index;                         //index for the stream
	uint8_t ct_index = 0;                     //index for chunked array
	uint8_t rcvBuff[DATA_LEN];                //stream buffer
	uint8_t capturedata[DATA_LEN];
	char temp[DATA_LEN];	                  //temporary character array
	uint16_t dataBuff[VAR_LEN];
	uint16_t txBuff[VAR_LEN];
	uint8_t v_index = 0;	
	datacomp_t ave;
	
	#ifdef DEBUG
	void setLeds(uint8_t value);
	#endif

	/********************** TASKS AND FUNCTIONS ***********************/
	/* -----------------------------------------------------------------
		task definition for formatting data (fmtData)
			* format data to fit DataMsg structure
	----------------------------------------------------------------- */
	task void fmtData(void){
		double v, i, p, q, s, f, t;
		call Resource.release();
		
		ct_index = 0; v_index = 0;

		//convert character array into nx_uint16_t
		for (rx_index = 0; rx_index < DATA_LEN; rx_index++){            
			if ((rcvBuff[rx_index] == '&')||(rcvBuff[rx_index]=='.')){
				temp[ct_index] = '\0';
				dataBuff[v_index] = (uint16_t)(atoi(temp));
				v_index++; ct_index = 0;
			}
			else{
				temp[ct_index] = rcvBuff[rx_index];
				ct_index++;
			}
		}
		
		//convert values to double
		v = (double)dataBuff[0] + (double)dataBuff[1]/100.0;
		i = (double)dataBuff[2] + (double)dataBuff[3]/100.0;
		p = (double)dataBuff[4] + (double)dataBuff[5]/100.0;
		q = (double)dataBuff[6] + (double)dataBuff[7]/100.0;
		s = (double)dataBuff[8] + (double)dataBuff[9]/100.0;
		f = (double)dataBuff[10] + (double)dataBuff[11]/100.0;
		t = (double)dataBuff[12] + (double)dataBuff[13]/100.0;
		
		//update data values
		ave.vrms = (ave.vrms * (double)ave.n + v)/((double)ave.n + 1.0);
		ave.irms = (ave.irms * (double)ave.n + i)/((double)ave.n + 1.0);
		ave.ppwr = (ave.ppwr * (double)ave.n + p)/((double)ave.n + 1.0);
		ave.qpwr = (ave.qpwr * (double)ave.n + q)/((double)ave.n + 1.0);
		ave.spwr = (ave.spwr * (double)ave.n + s)/((double)ave.n + 1.0);
		ave.freq = (ave.freq * (double)ave.n + f)/((double)ave.n + 1.0);
		ave.ph = (ave.ph * (double)ave.n + t)/((double)ave.n + 1.0);
	}

	task void transmitData(){
		double i;
		if(!busy){
			datamsg_t* transData = (datamsg_t*)call DataSender.getPayload(&pkt, sizeof(datamsg_t));
			if (transData == NULL) { 
				return;								//exit event
			}
			//prepare values for transmission
			txBuff[1] = (uint16_t)(modf(ave.vrms, &i)*100.0);
			txBuff[0] = (uint16_t)i;
			txBuff[3] = (uint16_t)(modf(ave.irms, &i)*100.0);
			txBuff[2] = (uint16_t)i;
			txBuff[5] = (uint16_t)(modf(ave.ppwr, &i)*100.0);
			txBuff[4] = (uint16_t)i;
			txBuff[7] = (uint16_t)(modf(ave.qpwr, &i)*100.0);
			txBuff[6] = (uint16_t)i;
			txBuff[9] = (uint16_t)(modf(ave.spwr, &i)*100.0);
			txBuff[8] = (uint16_t)i;
			txBuff[11] = (uint16_t)(modf(ave.freq, &i)*100.0);
			txBuff[10] = (uint16_t)i;
			txBuff[13] = (uint16_t)(modf(ave.ph, &i)*100.0);
			txBuff[12] = (uint16_t)i;
			
			//otherwise, get reading values for transmission
			transData->node_id = TOS_NODE_ID;
			transData->vrms_int = (uint8_t)txBuff[0];
			transData->vrms_frc = txBuff[1];
			transData->irms_int = (uint8_t)txBuff[2];
			transData->irms_frc = txBuff[3];
			transData->actp_int = txBuff[4];
			transData->actp_frc = txBuff[5];
			transData->reap_int = txBuff[6];
			transData->reap_frc = txBuff[7];
			transData->appp_int = txBuff[8];
			transData->appp_frc = txBuff[9];
			transData->freq_int = (uint8_t)txBuff[10];
			transData->freq_frc = txBuff[11];
			transData->ph_int = txBuff[12];
			transData->ph_frc = (uint8_t)txBuff[13];

			if(call DataSender.send(&pkt,sizeof(datamsg_t))==SUCCESS){
				busy = TRUE;
#ifdef DEBUG
				setLeds(4);
#endif
			}
		}

	}
	
#ifdef DEBUG
	void setLeds(uint8_t value){
		switch(value){
			case 0: call Leds.led0Off(); call Leds.led1Off(); call Leds.led2Off(); break;
			case 1: call Leds.led0On(); call Leds.led1Off(); call Leds.led2Off(); break;
			case 2: call Leds.led0Off(); call Leds.led1On(); call Leds.led2Off(); break;
			case 3: call Leds.led0On(); call Leds.led1On(); call Leds.led2Off(); break;
			case 4: call Leds.led0Off(); call Leds.led1Off(); call Leds.led2On(); break;
			case 5: call Leds.led0On(); call Leds.led1Off(); call Leds.led2On(); break;
			case 6: call Leds.led0Off(); call Leds.led1On(); call Leds.led2On(); break;
			case 7: call Leds.led0On(); call Leds.led1On(); call Leds.led2On(); break;
			default:
		}
	}
#endif

	/*************************** EVENTS *******************************/
	/* -----------------------------------------------------------------
		PROGRAM STARTS HERE: device completes booting up
	----------------------------------------------------------------- */
	event void Boot.booted() {
		call AMControl.start(); //initialize split-control for AM Radio
	}

	/* -----------------------------------------------------------------
		RADIO HARDWARE is ready for control
			* what to do when AMControl finished initialization
	----------------------------------------------------------------- */
	event void AMControl.startDone(error_t err) {
		if (err == SUCCESS) {
			call CollectionControl.start();
			
			call ReadPort.makeOutput();
			call ReadPort.clr();
			call InPort.makeInput();
			call Interrupt.enable();
			call Interrupt.edge(TRUE); //triggers to low-to-high transition
			call Counter.startPeriodic((unsigned)61440);
		}
		else	//if init fails, initialize split-control again
			call AMControl.start(); 
	}

	/* -----------------------------------------------------------------
		split-control event completed (callback)
			* what to do when AMControl finished stopping
	----------------------------------------------------------------- */
	event void AMControl.stopDone(error_t err) {
		call AMControl.start();
	}

	/* -----------------------------------------------------------------
		timer countdown completed (callback)
			* what to do when counter finishes 1 minute
	----------------------------------------------------------------- */
	event void Counter.fired(){
		post transmitData();
		ave.vrms = 0.0;
		ave.irms = 0.0;
		ave.ppwr = 0.0;
		ave.qpwr = 0.0;
		ave.spwr = 0.0;
		ave.freq = 0.0;
		ave.ph = 0.0;
		ave.n = 0;
	}
	
	/* -----------------------------------------------------------------
		TELOSB SUCCESSFULLY SENDS PACKET TO BASE STATION:
		clear flag to allow next transmission
			* what to do when AMSend finished transmission
	----------------------------------------------------------------- */
	event void DataSender.sendDone(message_t *msg, error_t ok) {
		//if the sent message is equal to the address of packet set for Tx
		//allow the mote to do next transmission
		 if (&pkt == msg) { 
			busy = FALSE;
		}
	}

	/* -----------------------------------------------------------------
		TIVA INTERRUPTS TELOSB
		send acknowledgement to RPI
			* what to do when Tiva successfully curtails
	----------------------------------------------------------------- */
	async event void Interrupt.fired(){
		call Interrupt.clear();
#ifdef DEBUG
		setLeds(6);
#endif
		//initiate UART request
		call Resource.request();
	}

	/* -----------------------------------------------------------------
		UART IS ALLOWED TO USE THE BUS RESOURCE:
		initiate receiving data from Tiva
			*when UART can actually be used
	----------------------------------------------------------------- */
	event void Resource.granted(){
		call ReadPort.set();
		call ReadPort.clr(); //Signal tiva to initiate data transfer
		
		call UartStream.receive(rcvBuff, DATA_LEN);

	}

	/* -----------------------------------------------------------------
				Uart Configuration
	----------------------------------------------------------------- */
#if defined(PLATFORM_TELOSB)
		msp430_uart_union_config_t msp430_uart_9600_config = {
		{
		// Baud rate (use enum msp430_uart_rate_t in msp430usart.h for predefined rates)
			ubr    : UBR_1MHZ_9600,	    
		// Modulation (use enum msp430_uart_rate_t in msp430usart.h for predefined rates)
			umctl  : UMCTL_1MHZ_9600,
		// Clock source (00=UCLKI; 01=ACLK; 10=SMCLK; 11=SMCLK)
			ssel   : 0x02,												
		// Parity enable (0=disabled; 1=enabled)
			pena   : 0,													
		// Parity select (0=odd; 1=even)
			pev    : 0,													
		// Stop bits (0=one stop bit; 1=two stop bits)
			spb    : 0,													
		// Character length (0=7-bit data; 1=8-bit data)
			clen   : 1,	   
		// Listen enable (0=disabled; 1=enabled, feed tx back to receiver)	
			listen : 0,													
		// Multiprocessor mode (0=idle-line protocol; 1=address-bit protocol)	
			mm     : 0,													
		// Clock polarity (0=normal; 1=inverted)	
			ckpl   : 0,													
		// Receive start-edge detection (0=disabled; 1=enabled)	
			urxse  : 0,													
		// Erroneous-character receive (0=rejected; 1=recieved and URXIFGx set)	
			urxeie : 1,													
		// Wake-up interrupt-enable (0=all characters set URXIFGx; 1=only address sets URXIFGx)	
			urxwie : 0,													
			utxe   : 1,			// 1:enable tx module
			urxe   : 1			// 1:enable rx module
		}//9600 baud, system clock, no parity, 1 stop bit, 8-bit data length
		};
#else
#error "Unknown platform"
#endif

	/* -----------------------------------------------------------------
		WHEN UART CONFIGURATION IS REQUESTED:
			* provide configuration set above
	----------------------------------------------------------------- */
	async command msp430_uart_union_config_t* Msp430UartConfigure.getConfig() {
		return &msp430_uart_9600_config;
	}

	/* -----------------------------------------------------------------
		RECEIVING A BYTE FROM UART: TelosB will not do anything
			* what to do when pin 4 (Rx) receives data
	----------------------------------------------------------------- */
	async event void UartStream.receivedByte(uint8_t byte) {
		//do nothing
	}

	/* -----------------------------------------------------------------
	RECEIVING STREAM OF DATA FROM UART:
		if entire data is received, format it to transData
			* format received data to be transmitted to base station
	----------------------------------------------------------------- */
	async event void UartStream.receiveDone(uint8_t *buf, uint16_t len, error_t error) {
		if (len == DATA_LEN) {
			memcpy(capturedata,buf,DATA_LEN);
#ifdef DEBUG
			setLeds(2);
#endif
			post fmtData();
		}
		else {
			//data captured will also be stored in rcvBuff
			call UartStream.receive(capturedata, DATA_LEN);            
		}
	}

	/* -----------------------------------------------------------------
	SENDING DATA THROUGH UART: will not be used
	----------------------------------------------------------------- */
	async event void UartStream.sendDone(uint8_t *buf, uint16_t len, error_t error){}
		//not used

	/* -----------------------------------------------------------------
		TELOSB RECEIVES A PACKET FROM OTHER MOTES:
			* what to do when mote receives packet from other mote
	------------------------------------------------------------------*/
	event message_t* Receive.receive(message_t* msg, void* payload, uint8_t len){
		return msg;
	}
	
	/* -----------------------------------------------------------------
	OTHER UART (INEVITABLE or "REQUIRED") EVENTS
	----------------------------------------------------------------- */
	
}/** end of implementation **/
