import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.text.DecimalFormat;

public class DataSigMsg extends net.tinyos.message.Message {

	/** The default size of this message type (data signal message) in bytes **/
	public static final int DEFAULT_MESSAGE_SIZE = 23;

	/** The Active Message type associated with this message;
	 *	 this Active Message type (AM_TYPE) is the id assigned to
	 *  the AM interface in <teloscode>AppC.nc.
	 */
	public static final int AM_TYPE = 143;

	/** Create a new CtlSigMsg of size 5 bytes */
	public DataSigMsg(){
		super(DEFAULT_MESSAGE_SIZE);
		amTypeSet(AM_TYPE);
	}

	public double get_volt(){
		return (double)get_vi() + (double)get_vf()/10000.0;
	}

	public double get_curr(){
		return (double)get_ii() + (double)get_if()/10000.0;
	}

	public double get_ppwr(){
		return (double)get_actpi()-2500 + (double)get_actpf()/10000.0;
	}

	public double get_qpwr(){
		return (double)get_reapi()-2500 + (double)get_reapf()/10000.0;
	}

	public double get_spwr(){
		return (double)get_apppi()-2500 + (double)get_apppf()/10000.0;
	}

	public double get_freq(){
		return (double)get_freqi() + (double)get_freqf()/10000.0;
	}

	public double get_ph(){
		return (double)get_phi()-360 + (double)get_phf()/10000.0;
	}

	public double get_batt(){
		return (double)get_bvi() + (double)get_bvf()/10000.0;
	}

	public double get_soc(){
		return (double)get_soci() + (double)get_socv()/1000.0;
	}

	public String toString(){
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
                DecimalFormat df = new DecimalFormat("#.00");
		String s = "";
                try{
                        s = dtf.format(now) + "    ";
                        s += df.format(get_node()) + "    ";
                        s += df.format(get_volt()) + "    ";
                        s += df.format(get_curr()) + "    ";
                        s += df.format(get_ppwr()) + "    ";
			s += df.format(get_qpwr()) + "    ";
			s += df.format(get_spwr()) + "    ";
			s += df.format(get_freq()) + "    ";
			s += df.format(get_ph()) + "     ";
			s += df.format(get_batt()) + "      ";
			s += df.format(get_soc());
                }
                catch(Exception e){
                }
		return s;
	}

        ///////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'node_id'
	//    all fields type: int, unsigned
	//    offset (bits): 0
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////

	/*
	 * Return the offset(in bytes) of the field 'node
	 */
	public static int offset_node(){
		return(0 / 8);
	}

	/**
	  * Return the offset(in bits) of the field 'node'
	*/
	public static int offsetBits_node(){
		return 0;
	}

	/**
	 * Return the value (as an int) of the field 'node'
	 */
	public int get_node(){
		return (int)getUIntBEElement(offsetBits_node(),8);
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'vint'
	//    all fields type: int, unsigned
	//    offset (bits): 8
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////

	 /**
	  * Return the offset(in bytes) of the field 'vi'
	  */
	 public static int offset_vi(){
		 return(8 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'vi'
	  */
	 public static int offsetBits_vi(){
		 return 8;
	 }

	/**
	 * Return the value (as an int) of the field 'vi'
	 */
	public int get_vi(){
		 return (int)getUIntBEElement(offsetBits_vi(),8);
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'vf'
	//    all fields type: int, unsigned
	//    offset (bits): 16
	//    size(bits): 16
	////////////////////////////////////////////////////////////////////

	 /**
	  * Return the offset(in bytes) of the field 'vf'
	  */
	 public static int offset_vf(){
		 return(16 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'vf'
	  */
	 public static int offsetBits_vf(){
		 return 16;
	 }

	/**
	 * Return the value (as an int) of the field 'vf'
	 */
	public int get_vf(){
		 return (int)getUIntBEElement(offsetBits_vf(),16);
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'ii'
	//    all fields type: int, unsigned
	//    offset (bits): 32
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////

	 /**
	  * Return the offset(in bytes) of the field 'ii'
	  */
	 public static int offset_ii(){
		 return(32 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'ii'
	  */
	 public static int offsetBits_ii(){
		 return 32;
	 }

	/**
	 * Return the value (as an int) of the field 'ii'
	 */
	public int get_ii(){
		 return (int)getUIntBEElement(offsetBits_ii(),8);
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'if'
	//    all fields type: int, unsigned
	//    offset (bits): 40
	//    size(bits): 16
	////////////////////////////////////////////////////////////////////
	 /**
	  * Return the offset(in bytes) of the field 'if'
	  */
	 public static int offset_if(){
		 return(40 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'if'
	  */
	 public static int offsetBits_if(){
		 return 40;
	 }

	/**
	 * Return the value (as an int) of the field 'if'
	 */
	public int get_if(){
		 return (int)getUIntBEElement(offsetBits_if(),16);
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'actpi'
	//    all fields type: int, unsigned
	//    offset (bits): 56
	//    size(bits): 16
	////////////////////////////////////////////////////////////////////

	 /**
	  * Return the offset(in bytes) of the field 'actpi'
	  */
	 public static int offset_actpi(){
		 return(56 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'actpi'
	  */
	 public static int offsetBits_actpi(){
		 return 56;
	 }

	/**
	 * Return the value (as an int) of the field 'actpi'
	 */
	public int get_actpi(){
		 return (int)getUIntBEElement(offsetBits_actpi(),16);
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'actpf'
	//    all fields type: int, unsigned
	//    offset (bits): 72
	//    size(bits): 16
	////////////////////////////////////////////////////////////////////

	 /**
	  * Return the offset(in bytes) of the field 'actpf'
	  */
	 public static int offset_actpf(){
		 return(72 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'actpf'
	  */
	 public static int offsetBits_actpf(){
		 return 72;
	 }

	/**
	 * Return the value (as an int) of the field 'actpf'
	 */
	public int get_actpf(){
		 return (int)getUIntBEElement(offsetBits_actpf(),16);
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'reapi'
	//    all fields type: int, unsigned
	//    offset (bits): 88
	//    size(bits): 16
	////////////////////////////////////////////////////////////////////

	 /**
	  * Return the offset(in bytes) of the field 'reapi'
	  */
	 public static int offset_reapi(){
		 return(88 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'reapi'
	  */
	 public static int offsetBits_reapi(){
		 return 88;
	 }

	/**
	 * Return the value (as an int) of the field 'reapi'
	 */
	public int get_reapi(){
		 return (int)getUIntBEElement(offsetBits_reapi(),16);
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'reapf'
	//    all fields type: int, unsigned
	//    offset (bits): 104
	//    size(bits): 16
	////////////////////////////////////////////////////////////////////

	 /**
	  * Return the offset(in bytes) of the field 'reapf'
	  */
	 public static int offset_reapf(){
		 return(104 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'reapf'
	  */
	 public static int offsetBits_reapf(){
		 return 104;
	 }

	/**
	 * Return the value (as an int) of the field 'reapf'
	 */
	public int get_reapf(){
		 return (int)getUIntBEElement(offsetBits_reapf(),16);
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'apppi'
	//    all fields type: int, unsigned
	//    offset (bits): 120
	//    size(bits): 16
	////////////////////////////////////////////////////////////////////

	 /**
	  * Return the offset(in bytes) of the field 'apppi'
	  */
	 public static int offset_apppi(){
		 return(120 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'apppi'
	  */
	 public static int offsetBits_apppi(){
		 return 120;
	 }

	/**
	 * Return the value (as an int) of the field 'apppi'
	 */
	public int get_apppi(){
		 return (int)getUIntBEElement(offsetBits_apppi(),16);
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'apppf'
	//    all fields type: int, unsigned
	//    offset (bits): 136
	//    size(bits): 16
	////////////////////////////////////////////////////////////////////

	 /**
	  * Return the offset(in bytes) of the field 'apppf'
	  */
	 public static int offset_apppf(){
		 return(136 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'apppf'
	  */
	 public static int offsetBits_apppf(){
		 return 136;
	 }

	/**
	 * Return the value (as an int) of the field 'apppf'
	 */
	public int get_apppf(){
		 return (int)getUIntBEElement(offsetBits_apppf(),16);
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'freqi'
	//    all fields type: int, unsigned
	//    offset (bits): 152
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////

	 /**
	  * Return the offset(in bytes) of the field 'freqi'
	  */
	 public static int offset_freqi(){
		 return(152 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'freqi'
	  */
	 public static int offsetBits_freqi(){
		 return 152;
	 }

	/**
	 * Return the value (as an int) of the field 'freqi'
	 */
	public int get_freqi(){
		 return (int)getUIntBEElement(offsetBits_freqi(),8);
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'freqf'
	//    all fields type: int, unsigned
	//    offset (bits): 160
	//    size(bits): 16
	////////////////////////////////////////////////////////////////////

	 /**
	  * Return the offset(in bytes) of the field 'freqf'
	  */
	 public static int offset_freqf(){
		 return(160 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'freqf'
	  */
	 public static int offsetBits_freqf(){
		 return 160;
	 }

	/**
	 * Return the value (as an int) of the field 'freqf'
	 */
	public int get_freqf(){
		 return (int)getUIntBEElement(offsetBits_freqf(),16);
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'phi'
	//    all fields type: int, unsigned
	//    offset (bits): 176
	//    size(bits): 16
	////////////////////////////////////////////////////////////////////

	 /**
	  * Return the offset(in bytes) of the field 'phi'
	  */
	 public static int offset_phi(){
		 return(176 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'phi'
	  */
	 public static int offsetBits_phi(){
		 return 176;
	 }

	/**
	 * Return the value (as an int) of the field 'phi'
	 */
	public int get_phi(){
		 return (int)getUIntBEElement(offsetBits_phi(),16);
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'phf'
	//    all fields type: int, unsigned
	//    offset (bits): 192
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////

	 /**
	  * Return the offset(in bytes) of the field 'phf'
	  */
	 public static int offset_phf(){
		 return(192 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'phf'
	  */
	 public static int offsetBits_phf(){
		 return 192;
	 }

	/**
	 * Return the value (as an int) of the field 'phf'
	 */
	public int get_phf(){
		 return (int)getUIntBEElement(offsetBits_phf(),8);
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'bvi'
	//    all fields type: int, unsigned
	//    offset (bits): 200
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////

	 /**
	  * Return the offset(in bytes) of the field 'bvi'
	  */
	 public static int offset_bvi(){
		 return(200 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'bvi'
	  */
	 public static int offsetBits_bvi(){
		 return 200;
	 }

	/**
	 * Return the value (as an int) of the field 'bvi'
	 */
	public int get_bvi(){
		 return (int)getUIntBEElement(offsetBits_bvi(),8);
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'bvf'
	//    all fields type: int, unsigned
	//    offset (bits): 208
	//    size(bits): 16
	////////////////////////////////////////////////////////////////////

	 /**
	  * Return the offset(in bytes) of the field 'bvf'
	  */
	 public static int offset_bvf(){
		 return(208 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'bvf'
	  */
	 public static int offsetBits_bvf(){
		 return 208;
	 }

	/**
	 * Return the value (as an int) of the field 'bvf'
	 */
	public int get_bvf(){
		 return (int)getUIntBEElement(offsetBits_bvf(),16);
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'soci'
	//    all fields type: int, unsigned
	//    offset (bits): 224
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////

	 /**
	  * Return the offset(in bytes) of the field 'soci'
	  */
	 public static int offset_soci(){
		 return(224 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'soci'
	  */
	 public static int offsetBits_soci(){
		 return 224;
	 }

	/**
	 * Return the value (as an int) of the field 'soci'
	 */
	public int get_soci(){
		 return (int)getUIntBEElement(offsetBits_soci(),8);
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'socv'
	//    all fields type: int, unsigned
	//    offset (bits): 232
	//    size(bits): 16
	////////////////////////////////////////////////////////////////////

	 /**
	  * Return the offset(in bytes) of the field 'socv'
	  */
	 public static int offset_socv(){
		 return(232 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'socv'
	  */
	 public static int offsetBits_socv(){
		 return 232;
	 }

	/**
	 * Return the value (as an int) of the field 'socv'
	 */
	public int get_socv(){
		 return (int)getUIntBEElement(offsetBits_socv(),16);
	}

	/////////////////////////////////////////////////////////////
	//	Accessor methods for field 'ack'
	//	all fields type: int
	//	offset: 248
	//	size: 8
	////////////////////////////////////////////////////////////

	/**
	 * Return the offset (in bytes) of the field 'ack'
         */
	public static int offset_ack(){
		return(248/8);
	}

	/**
	 * Return the offset of the field 'ack' in bits
	 */
	public static int offsetBits_ack(){
		return 248;
	}

	/**
	 * Retrieves the value of 'ack'
	 */
	public int get_ack(){
		return (int)getUIntBEElement(offsetBits_ack(),8);
	}
}

