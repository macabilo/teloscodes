#include "printf.h"

configuration InterruptTestAppC{}

implementation{
	components MainC, LedsC, InterruptTestC as App;
	components HplMsp430GeneralIOC as GPIOC;
	components HplMsp430InterruptP as IntC;
	App.Boot -> MainC;
	App.Leds -> LedsC;

	App.OutPin -> GPIOC.Port26;
	App.InPin -> GPIOC.Port23;
	App.Interrupt -> IntC.Port23;

	components PrintfC;
	components SerialStartC;
}
