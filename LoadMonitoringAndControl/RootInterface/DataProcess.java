import java.io.IOException;
import java.sql.*;

import net.tinyos.message.*;
import net.tinyos.packet.*;
import net.tinyos.util.*;

public class DataProcess implements MessageListener{

	private MoteIF moteIF;

	static final String driver = "com.mysql.jdbc.Driver";
	static final String url = "jdbc:mysql://localhost/helios_db";

	static final String username = "helios";
	static final String password = "apollo";
	
	private int current_log = 0;
	
	public DataProcess(MoteIF moteIF){
		this.moteIF = moteIF;
		this.moteIF.registerListener(new PowerSigMsg(), this);
	}

	private int assignUpdateVal(int formervalue){
		if (formervalue < 255)
			return formervalue + 1
		else
			return 0
	}
	
	public CtlSigMsg updateNetwork(CtlSigMsg payload){
		Connection conn = null;
		Statement stmt = null;

		try{
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, username, password);
			stmt = conn.createStatement();
			String sql = "SELECT * FROM dr_data ORDER BY logId DESC LIMIT 1";
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()){
				if (rs.getInt(1) != current_log){
					current_log = rs.getInt(1);         //log_ID: for detecting if new control signal has been saved in the database
					payload.set_req(assignUpdateVal(payload.get_req()));
					payload.set_ctrl1(rs.getInt(6));  //ctl1
					payload.set_ctrl2(rs.getInt(7));  //ctl2
					payload.set_ctrl3(rs.getInt(8));  //ctl3
					payload.printPacketString();
					try{
						moteIF.send(0, payload);
						count++;
					}
					catch (IOException ex){
						System.err.println("Error in sending packet to basemote: " + ex);
					}
					
					System.out.println("===================================");
					System.out.print(" New ControlSignal: ");
					System.out.println(payload.toString());
					System.out.println("===================================");
					System.out.println("[Date]    [Time]    [NodeID]    [Vrms]    [Irms]   [Real P]    [Reac P]    [Apparent]   [Freq]    [Ph]     [Batt]     [SOC]  ");
				}
				else{
					System.out.println("===================================");
					System.out.print(" No new control signal ");
					System.out.println(payload.toString());
					System.out.println("===================================");
					System.out.println("[Date]    [Time]    [NodeID]    [Vrms]    [Irms]   [Real P]    [Reac P]    [Apparent]   [Freq]    [Ph]     [Batt]     [SOC]  ");
				}
			}
		}
		catch(SQLException se){
			se.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			try{
				if (stmt != null)
					conn.close();
			}catch(SQLException se){}
			try{
				if(conn != null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}
		}
	}
	
	public void messageReceived(int to, Message message){
		Connection conn = null;
		Statement stmt = null;
		PowerSigMsg msg = (PowerSigMsg)message;

		try{
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, username, password);
			stmt = conn.createStatement();
			String sql = " "
			if (msg.get_node() == 10){
				sql = "INSERT INTO battery_data VALUES (NULL, CURDATE(), CURTIME(), " +
					Long.toString(msg.get_batt()) + "," +
					Long.toString(msg.get_soc()) + ")";
			}
			else{
				sql = "INSERT INTO lmcu_data VALUES (NULL, "+ Long.toString(msg.get_node()) + ",  CURDATE(), CURTIME()," + 
					Long.toString(msg.get_volt()) + "," +
					Long.toString(msg.get_curr()) + "," +
					Long.toString((msg.get_ppwr()) + "," +
					Long.toString((msg.get_qpwr()) + "," +
					Long.toString((msg.get_spwr()) + "," +
					Long.toString(msg.get_freq()) + "," +
					Long.toString((msg.get_ph()) + ")";
			}
			stmt.executeUpdate(sql);
		}
		catch(SQLException se){
			se.printStackTrace();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		finally{
			try {
				if (stmt != null)
					conn.close();
			}catch(SQLException se){}
			try{
				if(conn != null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}
		}
		msg.printPacketStringDbl();
	}

	private static void usage(){
		System.err.println("usage: DataProcess [-begin] [<source>]");
	}

	public static void main(String[] args) throws Exception{
		String source = null;

		if (args.length == 2){
			if (!args[0].equals("-begin")){
				usage();
				System.exit(1);
			}
			source = args[1];
		}
		else{
			usage();
			System.exit(1);
		}

		PhoenixSource phoenix;

		if (source == null){
			phoenix = BuildSource.makePhoenix(PrintStreamMessenger.err);
		}
		else{
			phoenix = BuildSource.makePhoenix(source, PrintStreamMessenger.err);
		}

		MoteIF mif = new MoteIF(phoenix);
		DataProcess basemote = new DataProcess(mif);
		CtlSigMsg payload = new CtlSigMsg();
		System.out.println("[Date]    [Time]    [NodeID]    [Vrms]    [Irms]   [Real P]    [Reac P]    [Apparent]   [Freq]    [Ph]     [Batt]     [SOC]  ");

		while(1){
			payload = updateNetwork(payload);
			Thread.sleep(10240);
		}
	}
}
