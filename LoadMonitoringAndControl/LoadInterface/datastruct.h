

#ifndef DATASTRUCT_H
#define DATASTRUCT_H

#define DATA_REQ 20
#define LOAD_MNG 44
#define DO_NOTHING 33
#define TIVA_ACK 78

//#define SMU
#define LMCU
enum {
    AM_TIVATRANSMIT = 6,
    DATA_LEN = 79, //serial packet length (Tiva-Telos Transmission)
    VAR_LEN = 18
};

typedef nx_struct data_msg{
    nx_uint8_t node_id;
    nx_uint8_t vrms_int;
    nx_uint16_t vrms_frc;
    nx_uint8_t irms_int;
    nx_uint16_t irms_frc;
    nx_uint16_t actp_int;
    nx_uint16_t actp_frc;
    nx_uint16_t reap_int;
    nx_uint16_t reap_frc;
    nx_uint16_t appp_int;
    nx_uint16_t appp_frc;
    nx_uint8_t freq_int;
    nx_uint16_t freq_frc;
    nx_uint16_t ph_int;
    nx_uint8_t ph_frc;
    nx_uint8_t batt_int;
    nx_uint16_t batt_frc;
    nx_uint8_t soc_int;
    nx_uint16_t soc_frc;
    nx_uint8_t ack;
} datamsg_t;

typedef nx_struct control_msg{
    nx_uint8_t unique_identifier;
    nx_uint8_t cs_type;
    nx_uint8_t value;
}controlmsg_t;

#endif
