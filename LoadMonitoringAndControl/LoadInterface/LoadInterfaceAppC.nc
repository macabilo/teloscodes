#include "datastruct.h"

configuration LoadInterfaceAppC {
}

implementation {
  //main components
  components LoadInterfaceC as App;
  components MainC;
  App.Boot -> MainC;

#ifdef DEBUG
  components LedsC;
  App.Leds -> LedsC;
#endif
  
  //GPIO components
  components HplMsp430GeneralIOC;
#ifndef SMU
  components HplMsp430InterruptC;
#endif
	
#ifdef SMU
  App.ReadPort -> HplMsp430GeneralIOC.Port26; //GIO3
#else
  //LMCU
  App.ReadPort -> HplMsp430GeneralIOC.Port21; //For output- GIO1
  App.CurtPort -> HplMsp430GeneralIOC.Port23; //For output - GIO2	
  App.InPort -> HplMsp430GeneralIOC.Port26; //For input - GIO3
  App.Interrupt -> HplMsp430InterruptC.Port26; //For input - GIO3
#endif

  //UART components
  components new Msp430Uart0C() as Uart;
  App.UartStream -> Uart.UartStream;
  App.Resource -> Uart.Resource;
  App.Msp430UartConfigure <- Uart.Msp430UartConfigure;
  
  //Split-control components
  components ActiveMessageC;
  App.AMControl -> ActiveMessageC;

  //Radio Tx components

  components CollectionC;
  components new CollectionSenderC(AM_TIVATRANSMIT) as DataSender;
  App.CollectionControl -> CollectionC;
  App.DataSender -> DataSender;
  App.LowPowerListening -> ActiveMessageC;

  components DisseminationC;
  components new DisseminatorC(controlmsg_t, 3);
  App.DisseminationControl -> DisseminationC;
  App.SignalValue -> DisseminatorC;
  
  //Radio Rx components
  components new AMReceiverC(AM_TIVATRANSMIT);
  App.Receive -> AMReceiverC;
}
