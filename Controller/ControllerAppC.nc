
#include "packetdata.h"
#include "printf.h"

configuration ControllerAppC {}

implementation{
	components ControllerC as App, MainC, SerialActiveMessageC as Serial;
	App.Boot -> MainC;
	App.UartReceive -> Serial.Receive[143];
	
	components ActiveMessageC as Radio;
	App.AMControl -> Radio;

	components DisseminationC;
	components new DisseminatorC(sig_t, 3);
	App.DisseminationControl -> DisseminationC;
	App.SignalUpdate -> DisseminatorC;

	components PrintfC;
	components SerialStartC;
}
