import java.io.IOException;
import net.tinyos.message.*;
import net.tinyos.packet.*;
import net.tinyos.util.*;

public class MainController implements MessageListener{

	private MoteIF moteIF;

	public MainController(MoteIF moteIF){
		this.moteIF = moteIF;
		this.moteIF.registerListener(new SigMsg(), this);
	}

	public void messageReceived(int to, Message message){}

	public static void main(String[] args) throws Exception{
		String source = null;

		if (args.length == 2){
			if(args[0].equals("-begin")){
				source = args[1];
			}
			else{
				System.exit(1);
			}
		}
		else{
			System.exit(1);
		}
		PhoenixSource phoenix;

		if(source == null){
			phoenix = BuildSource.makePhoenix(PrintStreamMessenger.err);
		}
		else{
			phoenix = BuildSource.makePhoenix(source, PrintStreamMessenger.err);
		}
		MoteIF mif = new MoteIF(phoenix);
		MainController base = new MainController(mif);
		int count = 0;
		SigMsg msg = new SigMsg();
		msg.set_node(8);
		while(true){
			if (count == 3)
				count = 1;
			else
				count = count + 1;
			msg.set_value(count);
			try{
				System.out.println("Sending control signal with count = " + Long.toString(count));
				mif.send(10,msg);
				System.out.println("Message sent!");
			}
			catch(IOException ioe){
				System.out.println("Error sending packet");
			}
			Thread.sleep(1024);
		}
	}
}
