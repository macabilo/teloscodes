
/**
 @title:		EndMoteC.nc
 @author:		Mark Anthony Cabilo
 @date began:		December 2017
 @date finished:	
 @description:		Interfaces sensors and actuators to a wireless sensor network
**/

#include "datastruct.h"

module EndMoteC {
	//provides interface Msp430UartConfigure;
	uses{ //fundamental interfaces
		interface Boot;
		interface SplitControl as AMControl;
		interface Leds;
	}
	
	uses{ //communication interfaces
		interface StdControl as CollectionControl;
		interface StdControl as DisseminationControl;
		interface DisseminationValue<CtlSig_t> as SignalValue;
		interface LowPowerListening;
		interface Send as DataSender;
		interface Receive;
	}
	
	uses{ //GPIO interfaces
		interface HplMsp430GeneralIO as InPort; //pin 3 in 6-pin GPIO
		interface HplMsp430GeneralIO as OutPort; //pin 4 in 6-pin GPIO
		interface HplMsp430Interrupt as Interrupt; 
	}
}

implementation{
	CtlSig_t* data;
	message_t pkt;
	bool busy = FALSE; 
	uint8_t ctlval = 0;
	bool firstTime = TRUE;
	uint8_t sensorState = 0;
	
	void setLeds(uint8_t value){
		switch(value){
			case 0:	call Leds.led0Off(); call Leds.led1Off(); call Leds.led2Off(); break;
			case 1:	call Leds.led0On(); call Leds.led1Off(); call Leds.led2Off(); break;
			case 2:	call Leds.led0Off(); call Leds.led1On(); call Leds.led2Off(); break;
			case 3:	call Leds.led0On(); call Leds.led1On(); call Leds.led2Off(); break;
			case 4:	call Leds.led0Off(); call Leds.led1Off(); call Leds.led2On(); break;
			case 5: call Leds.led0On(); call Leds.led1Off(); call Leds.led2On(); break;
			case 6: call Leds.led0Off(); call Leds.led1On(); call Leds.led2On(); break;
			case 7: call Leds.led0On(); call Leds.led1On(); call Leds.led2On(); break;
			default:
		}
	}
	
	task void fmtData(void){
		if (!busy){
			data = (CtlSig_t*)call DataSender.getPayload(&pkt, sizeof(CtlSig_t));
			if (data == NULL){
				return;
			}
			else{
				data->node_id = TOS_NODE_ID;
				data->status = (sensorState << TOS_NODE_ID); //toggle state for sensor
				
				if (call DataSender.send(&pkt, sizeof(CtlSig_t))==SUCCESS){
					busy = TRUE;
				}
			}
		}
	}
	
	event void Boot.booted(){
		call AMControl.start();
	}
	
	event void AMControl.startDone(error_t status){
		if (status == SUCCESS){
			call CollectionControl.start();
			call OutPort.makeOutput();
			call OutPort.clr();

			call InPort.makeInput();

			call Leds.led0Off();
			call Leds.led1Off();
			call Leds.led2Off();
			
			call DisseminationControl.start();
			
			call Interrupt.enable();
			call Interrupt.edge(TRUE);
		}
		else{
			call AMControl.start();
		}
	}
	
	event void AMControl.stopDone(error_t status){
		call AMControl.start();
	}
	
	async event void Interrupt.fired(){
		volatile static bool fs;
		call Interrupt.clear();
		setLeds(ARDUINO_TO_TELOS);

		switch(TOS_NODE_ID){
			case 1:
				if(sensorState == 0){
					sensorState = 1;
					fs = TRUE;		
				}
				break;
			case 2:
				if(sensorState == 1){
					sensorState = 0;
					fs = TRUE;
				}
				break;
			case 3:
			case 4: break;
			case 5: 
				if(sensorState == 1){
					sensorState = 0;
					fs = TRUE;
				}
				break;
			default:
		}
		if (fs){
			fs = FALSE;
			post fmtData();
		}
	}

	event void DataSender.sendDone(message_t* msg, error_t status){
		if (&pkt == msg){
			setLeds(ARDUINO_TO_RPI);
			busy = FALSE;
		}
	}
	
	event message_t* Receive.receive(message_t* msg, void* payload, uint8_t length){
		return msg;
	}
	
	event void SignalValue.changed(){
		const CtlSig_t* cs = call SignalValue.get();

		//initial control signal - assignment of sensor states
		if((cs->status & 128)>>7){
			setLeds(INITIAL_STATE);
			sensorState = ((cs->status & (1<<TOS_NODE_ID))>>TOS_NODE_ID);
		}
		//control signal sent from RPI in response to any sensor
		else{
			setLeds(RPI_TO_TELOS);
			//if control signal toggles arduino sensor, change sensor state and send interrupt to arduino
			
			//else, do nothing
			if (sensorState == ((cs->status & (1<<TOS_NODE_ID))>>TOS_NODE_ID)){
				
			}
			else{
				setLeds(RPI_TO_TELOS);
				sensorState ^= (1<<0);
				call OutPort.set();
				call OutPort.clr();
			}
		}
	}
	
}
