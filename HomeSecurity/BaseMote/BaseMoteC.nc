#include "datastructbase.h"

module BaseMoteC {
	uses{
		interface Boot;
		interface SplitControl as SerialControl;
		interface SplitControl as RadioControl;
	}
	uses{
		interface StdControl as CollectionControl;
		interface RootControl;
		interface Receive as CollectionReceive[am_id_t id];
		interface Packet as RadioPacket;
		interface CollectionPacket;
		
		interface DisseminationUpdate<CtlSig_t> as SignalUpdate;
		interface StdControl as DisseminationControl;
	}
	
	uses{
		interface Receive as UartReceive;
		interface AMSend as UartSend;
		interface Packet as UartPacket;
		interface AMPacket as UartAMPacket;
		
		interface Queue<message_t*>;
		interface Pool<message_t>;
	}
}

implementation{
	message_t packet;
	bool fwdBusy = FALSE;
	message_t fwdMsg;
	message_t uartmsg;
	
	task void serialForwardTask(void){
		if(!call Queue.empty() && !fwdBusy){
			message_t* msg = call Queue.dequeue();
			uint8_t length = call RadioPacket.payloadLength(msg);
			void* radio_payload = call RadioPacket.getPayload(msg, length);
			am_addr_t src = call CollectionPacket.getOrigin(msg);
			
			if (radio_payload != NULL){
				void* uart_payload;
				memcpy(&uartmsg, radio_payload, length);
				
				call UartPacket.clear(msg); 
				call UartAMPacket.setSource(msg, src);
				uart_payload = call UartPacket.getPayload(msg, length);
				if (uart_payload != NULL){
					memcpy(uart_payload, &uartmsg, length);
					
					if(call UartSend.send(AM_BROADCAST_ADDR, msg, length)==SUCCESS){
						fwdBusy = TRUE;
					}
					
				}
			}
		}
	}
	
	event void Boot.booted(){
		call SerialControl.start();
		call RadioControl.start();
	}
	
	event void SerialControl.startDone(error_t status){
		fwdBusy = FALSE;
		
		if (status == SUCCESS){
			if (!call Queue.empty()) //if there is pending message in queue, forward the message serially
				post serialForwardTask();
		}
		else{
			call SerialControl.start();
		}
	}
	
	event void SerialControl.stopDone(error_t e){}
	
	event void RadioControl.startDone(error_t status){
		if (status == FAIL){
			call RadioControl.start();
		}
		else{
			call CollectionControl.start();
			call DisseminationControl.start();
			call RootControl.setRoot();
		}
	}
	
	event void RadioControl.stopDone(error_t e){}
	
	event message_t* CollectionReceive.receive[collection_id_t id](message_t* msg, void* payload, uint8_t length){
		if (!call Pool.empty() && call Queue.size() < call Queue.maxSize()){
			message_t* temp = call Pool.get();
			call Queue.enqueue(msg);
			if (!fwdBusy){
				post serialForwardTask();
			}
			return temp;
		}
		return msg;
	}
	
	event message_t* UartReceive.receive (message_t* msg, void* payload, uint8_t length){
		if (length != sizeof(CtlSig_t)){ return msg;}
		else{
			CtlSig_t* ctlval = (CtlSig_t*)payload;
			
			call SignalUpdate.change(ctlval);
			return msg;
		}
	}
	
	event void UartSend.sendDone(message_t* msg, error_t status){
		fwdBusy = FALSE;
		call Pool.put(msg);
		if (!call Queue.empty())
			post serialForwardTask();
	}
}
