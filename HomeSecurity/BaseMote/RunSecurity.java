import java.io.IOException;
import java.sql.*;

import net.tinyos.message.*;
import net.tinyos.packet.*;
import net.tinyos.util.*;

public class RunSecurity implements MessageListener{

	private MoteIF moteIF;  //mote handler/source?
	private static int packetCount = 0;

	static final String driver = "com.mysql.jdbc.Driver";
	static final String dbUrl = "jdbc:mysql://localhost/wsn-db";

	static final String username = "JEN";
	static final String password = "1209jen3487";
	private static int siren;

	public RunSecurity(MoteIF moteIF){ //instance constructor
		this.moteIF = moteIF; //setting instance parameter (1): mote source
		this.moteIF.registerListener(new CtlSigMsg(), this); //parameter (2): packet format to listen to and from what source
	}

	public void messageReceived(int to, Message message){
		Connection conn = null;
		Statement stmt = null;
		CtlSigMsg msg = (CtlSigMsg)message;
		int mote = msg.get_nodeID();
		int data = msg.get_status();
		String sql = " ";
		siren = 0;
		packetCount++;

		System.out.println("New data from node "+Long.toString(mote)+": "+Long.toString(data));
		try{
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(dbUrl, username, password);
			stmt = conn.createStatement();
			sql = "UPDATE sensor_data SET date=CURDATE(), time=CURTIME(), ardustat=0";
			switch(mote){
				case 1: //doorbell
				sql += ", sensor1=" + Long.toString((data & (1<<1))>>1);
				//signal camera capture
				break;
				case 2: //laser detector
				sql += ", sensor2=" + Long.toString((data & (1<<2))>>2);
				if (data == 0)
					sql+= ", sensor3=1";
				break;
				case 3: //siren
				sql += ", sensor3=" + Long.toString((data & (1<<3))>>3);
				break;
				case 4: //doorlock
				sql += ", sensor4=" + Long.toString((data & (1<<4))>>4);
				if (data == 0)
					sql+=", sensor3=1";
				break;
				case 5: //laser source
				sql += ", sensor5=" + Long.toString((data & (1<<5))>>5);
				if (data == 0)
					sql+= ", sensor3=1";
				break;
				default:
			}
			sql += " WHERE logID=1";
			stmt.executeUpdate(sql);
		}
		catch(SQLException se){
			se.printStackTrace();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		finally{
			try {
				if (stmt != null)
					conn.close();
			}catch(SQLException se){
			}
			try{
				if(conn != null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}
		}

		try{
			conn = DriverManager.getConnection(dbUrl, username, password);
			stmt = conn.createStatement();
			sql = "SELECT * FROM sensor_data LIMIT 1";
			ResultSet rs = stmt.executeQuery(sql);
			if(rs.next()){
				try{
					msg.set_status(rs.getInt(4)*128+rs.getInt(6)*32+rs.getInt(7)*16+rs.getInt(8)*8+rs.getInt(9)*4+rs.getInt(10)*2);
					System.out.println("Sending control in response to node "+Long.toString(mote)+": "+
						Long.toString(rs.getInt(10)) + " " + Long.toString(rs.getInt(9))+ " " + 
						Long.toString(rs.getInt(8)) + " " + Long.toString(rs.getInt(7)) + " " +
						Long.toString(rs.getInt(6)) + " #st:" + Long.toString(rs.getInt(4)));
					moteIF.send(0, msg);
					System.out.println("Packet sent!");
				}
				catch(IOException ioe){
					System.out.println("Error sending ctrl signal");
				}
			}
		}
		catch(SQLException e){
		}
		catch(Exception e){
		}
		finally{
			try{
				if(conn != null)
					conn.close();
			}
			catch(SQLException e){}
			try{
				if(stmt != null)
					conn.close();
			}
			catch(SQLException e){}
		}
	}

	private static void usage(){
		System.err.println("Usage: NetworkRun [-begin] [<source>]");
	}

	public static void main(String[] args) throws Exception{
		String source = null;

		if (args.length == 2){
			if (!args[0].equals("-begin")){
				usage();
				System.exit(1);
			}
			else{
				source = args[1];
			}
		}
		else if(args.length != 0){
			usage();
			System.exit(1);
		}

		PhoenixSource phoenix;

		if (source == null){
			phoenix = BuildSource.makePhoenix(PrintStreamMessenger.err);
		}
		else{
			phoenix = BuildSource.makePhoenix(source, PrintStreamMessenger.err);
		}


		MoteIF mif = new MoteIF(phoenix);

		//start system initial state
		String sql = " ";
		Connection conn = null;
		Statement stmt = null;
		CtlSigMsg msg = new CtlSigMsg();
		try{
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(dbUrl, username, password);
			stmt = conn.createStatement();
			sql = "UPDATE sensor_data SET date=CURDATE(), time=CURTIME(), sensor1 = 0, sensor2 = 1, sensor3 = 0, sensor4=1, sensor5=1, notifstat=0, ardustat=1 WHERE logID=1";
			stmt.executeUpdate(sql);
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			try{
				if(stmt != null)
					conn.close();
			}
			catch(SQLException e){}
			try{
				if(conn != null)
					conn.close();
			}
			catch(SQLException e){}
		}

		//send control signal
		try{
			Class.forName(driver).newInstance();
			conn=DriverManager.getConnection(dbUrl, username, password);
			stmt=conn.createStatement();
			sql="SELECT * FROM sensor_data LIMIT 1";
			ResultSet rs = stmt.executeQuery(sql);
			if(rs.next()){
				msg.set_status(rs.getInt(4)*128+rs.getInt(6)*32+rs.getInt(7)*16+rs.getInt(8)*8+rs.getInt(9)*4+rs.getInt(10)*2);
				try{
					System.out.println("Sending initial control signal");
					mif.send(0,msg);
					System.out.println("Initial signal sent:" + Long.toString(msg.get_status()));
				}
				catch(IOException e){
					System.out.println("Error sending signal!");
				}
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			try{
				if(conn != null)
					conn.close();
			}
			catch(SQLException e){
				e.printStackTrace();
			}
			try{
				if(stmt != null)
					conn.close();
			}
			catch(SQLException e){
				e.printStackTrace();
			}
		}
		//Initiate telosb sending messages to base mote
		RunSecurity basemote = new RunSecurity(mif);
	}

}
