#include "datastructbase.h"

configuration BaseMoteAppC {}

implementation{
	components BaseMoteC as App;
	components MainC;
	App.Boot -> MainC;
	
	components SerialActiveMessageC as Serial;
	App.SerialControl -> Serial;
	App.UartReceive -> Serial.Receive[143];
	App.UartSend -> Serial.AMSend[143];
	App.UartPacket -> Serial;
	App.UartAMPacket -> Serial;
	
	components new QueueC(message_t*, SERIAL_QUEUE_SIZE);
	components new PoolC(message_t, SERIAL_QUEUE_SIZE);
	App.Queue -> QueueC;
	App.Pool -> PoolC;
	
	components ActiveMessageC as Radio;
	App.RadioControl -> Radio;
	
	components CollectionC;
	App.CollectionControl -> CollectionC;
	App.RootControl -> CollectionC;
	App.CollectionReceive -> CollectionC.Receive;
	App.RadioPacket -> CollectionC;
	App.CollectionPacket -> CollectionC;
	
	components DisseminationC;
	components new DisseminatorC(CtlSig_t, 3);
	App.DisseminationControl -> DisseminationC;
	App.SignalUpdate -> DisseminatorC;
	
}
