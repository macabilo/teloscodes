
public class CtlSigMsg extends net.tinyos.message.Message{
	public static final int DEFAULT_MESSAGE_SIZE = 2; //default size of ctlsig in bytes

	//active message type associated with this message (no idea)
	public static final int AM_TYPE=143;

	//create a new ctlsig of size 1 bytes
	public CtlSigMsg(){
		super(DEFAULT_MESSAGE_SIZE);
		amTypeSet(AM_TYPE);
	}

	//returns the string equivalent of ctlsig
	public String toString(){
		String s = " ";
		int data;
		try {

		}
		catch(ArrayIndexOutOfBoundsException aioobe){
			s = "Index out of bounds. Recheck data.";
		}
		return s;
	}

	public void printPacketString(){
		String s = " ";
		try{
			printHex(get_status());
		}
		catch(ArrayIndexOutOfBoundsException aioobe){
			s = "Index out of bounds. Recheck data.";
		}
	}

	public void printHex(int b){
		String s = Long.toHexString(b & 0xff).toUpperCase(); //since b is always uint8_t
		if (b >= 0 && b < 16)
			System.out.print("0");
		System.out.print(s + " ");
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'nodeID'
	//    all fields type: int, unsigned
	//    offset (bits): 0
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////

	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_nodeID(){
		 return false;
	 }

	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_nodeID(){
		 return false;
	 }

	 /**
	  * Return the offset(in bytes) of the field 'nodeID'
	  */
	 public static int offset_nodeID(){
		 return(0 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'nodeID'
	  */
	 public static int offsetBits_nodeID(){
		 return 0;
	 }

	/**
	 * Return the value (as an int) of the field 'nodeID'
	 */
	public int get_nodeID(){
		 return (int)getUIntBEElement(offsetBits_nodeID(),8);
	}

	/**
	 * Set the value of the field 'nodeID'
	 */
	public void set_nodeID(int value){
		setUIntBEElement(offsetBits_nodeID(),8,value);
	}

	/**
	 * return the size (in bytes) of the field 'nodeID'
	 */
	public static int size_nodeID(){
		return (8/8);
	}

	/**
	 * Return the size (in bits) of the field 'nodeID'
	 */
	public static int sizeBits_nodeID(){
		return 8;
	}

	////////////////////////////////////////////////////////////////////
	//	Accessor methods for field 'status'
	//    all fields type: int, unsigned
	//    offset (bits): 8
	//    size(bits): 8
	////////////////////////////////////////////////////////////////////

	/**
	 * Return whether the fields are signed (false)
	 */
	 public static boolean isSigned_status(){
		 return false;
	 }

	 /**
	  * return whether the fields are arrays (false)
	  */
	 public static boolean isArray_status(){
		 return false;
	 }

	 /**
	  * Return the offset(in bytes) of the field 'status'
	  */
	 public static int offset_status(){
		 return(8 / 8);
	 }

	 /**
	  * Return the offset(in bits) of the field 'status'
	  */
	 public static int offsetBits_status(){
		 return 8;
	 }

	/**
	 * Return the value (as an int) of the field 'status'
	 */
	public int get_status(){
		 return (int)getUIntBEElement(offsetBits_status(),8);
	}

	/**
	 * Set the value of the field 'status'
	 */
	public void set_status(int value){
		setUIntBEElement(offsetBits_status(),8,value);
	}

	/**
	 * return the size (in bytes) of the field 'status'
	 */
	public static int size_status(){
		return (8/8);
	}

	/**
	 * Return the size (in bits) of the field 'status'
	 */
	public static int sizeBits_status(){
		return 8;
	}
}
